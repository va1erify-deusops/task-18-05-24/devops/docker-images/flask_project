FROM python:3.8-alpine
WORKDIR app
COPY . .
RUN  apk add libpq-dev gcc \
    && pip3 install -r requirements.txt \
    && pip3 install -r requirements-server.txt
CMD ["gunicorn", "app:app", "-b", "0.0.0.0:8000"]